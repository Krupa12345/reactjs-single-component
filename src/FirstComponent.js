import React, { Component } from "react";
import "./index.css";
class FirstComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
        lists: [
            {id: 1, name: 'A', color: 'Red'},
            {id: 2, name: 'B', color: 'Green'}
        ],
      countRed: '1',
      countGreen: '1',
      countBlue: '0',
      name: "",
      color: "Red",
      updateId: "0",
      lastid:3,
    };
    this.handleChangeColor = this.handleChangeColor.bind(this);
    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleResetForm = this.handleResetForm.bind(this);
    this.countBall = this.countBall.bind(this);
  }

  render() {
    return (
      <div>        
        <form onSubmit={this.handleSubmit}>
        <input
                type="hidden"
                value={this.state.updateId}
                placeholder="Enter Name"
                
            />
            <label>Name</label>
            <input
                type="text"
                value={this.state.name}
                placeholder="Enter Name"
                onChange={this.handleChangeName}
            />
            <br />
            <label>Color</label>
            <select className="form-select" value={this.state.color} onChange={this.handleChangeColor}>
                    <option value="Red">Red</option>
                    <option value="Green">Green</option>
                    <option value="Blue">Blue</option>
                </select>
            <br />
          <button type="submit" className="btn save-btn">
            Save
          </button>
          <button type="button" className="btn clear-btn" onClick={this.handleResetForm}>
            Clear
          </button>
        </form>
        <div className="dotClass">
            <span className="dotRed"><div className="lableclass">{ this.state.countRed }</div></span>
            <span className="dotBlue"><div className="lableclass">{ this.state.countBlue }</div></span>
            <span className="dotGreen"><div className="lableclass">{ this.state.countGreen }</div></span>
        </div>
        

        <h1> Table </h1>

        <table className="crud-table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Color</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {this.state.lists.map((item, index) => (
              <tr key={item.id + index}>
                <td> {item.name} </td>
                <td> {item.color} </td>                
                <td>
                  <button
                    className="btn save-btn"
                    onClick={() => this.update(item)}
                  >
                    Edit
                  </button>                  
                  <button
                    className="btn del-btn"  onClick={() => { if (window.confirm('Are you sure you wish to delete: '+item.name+'?')) this.delete(item) } }
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }

  countBall(){
    var redCount = 0;
    var blueCount = 0;
    var greenCount = 0;
    this.state.lists.map(function(elementOfArray, indexInArray) {
        if (elementOfArray.color === 'Red') {            
            redCount ++;
        }
        if (elementOfArray.color === 'Blue') {            
            blueCount ++;
        }
        if (elementOfArray.color === 'Green') {            
            greenCount ++;
        }
    });    
    this.setState({ countRed: redCount });
    this.setState({ countBlue: blueCount });
    this.setState({ countGreen: greenCount });
  }

  delete(e) {
    this.state.lists.splice(this.state.lists.indexOf(e), 1);
    this.setState({ lists: this.state.lists });
    this.countBall();
  }
  
  update(e) {
    this.setState({ updateId: e.id });
    this.setState(e);
  }
 
  handleChangeName(e) {
    this.setState({ name: e.target.value });
    }
    handleChangeColor(e) {    
        this.setState({ color: e.target.value });
    }
handleResetForm(e){
    this.setState({ name: '' });
    this.setState({ color: 'Red' });
    this.setState({ updateId: '' });
    this.countBall();
}
  handleSubmit(e) {
    e.preventDefault();
        if (this.state.name === '') {
            alert('Both Field Are Required !!!');
          return false;
        }
        if(this.state.color === ''){
            alert('Both Field Are Required !!!');
            return false;
        }
        if (this.state.updateId === '0' || this.state.updateId === ''){
            var added=false;
            var productID = this.state.name;
            this.state.lists.map(function(elementOfArray, indexInArray) {
                if (elementOfArray.name === productID) {            
                    added = true;
                }
            });            
            if(added){
                alert(productID + ' Name is already Exists..Please enter Unique name !!!');
                return false;
            }
            const newItem = {
                name: this.state.name,
                color: this.state.color,
                id: this.state.lastid
            };
            
            this.state.lists.unshift(newItem);
            this.setState({ lists: this.state.lists });
            
        }else{
            var added1=false;
            var productID1 = this.state.name;
            var updateId = this.state.updateId;
            this.state.lists.map(function(elementOfArray, indexInArray) {
                if (elementOfArray.name === productID1 && elementOfArray.id !== updateId) {            
                    added1 = true;
                }
            });            
            if(added1){
                alert(productID1 + ' Name is already Exists..Please enter Unique name !!!');
                return false;
            }

            const updateItem = {
                name: this.state.name,
                color: this.state.color,
                id: this.state.updateId,
              };
              const index = this.state.lists.findIndex(p => p.id === this.state.updateId)
              this.state.lists[index] = updateItem;
                if(index === -1) {

                } else {
                    this.state.lists[index] = updateItem;
                }
              this.setState({ lists: this.state.lists });

        }
        var lastid = parseInt(this.state.lastid) + parseInt(1);
        this.setState({ lastid: lastid });
        this.handleResetForm();
        this.countBall();
  }
}

export default FirstComponent;
