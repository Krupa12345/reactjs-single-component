import "./App.css";
import "./index.css";
import FirstComponent from "./FirstComponent";

function App() {
  return (
    <div className="Main">
      <FirstComponent />
    </div>
  );
}

export default App;